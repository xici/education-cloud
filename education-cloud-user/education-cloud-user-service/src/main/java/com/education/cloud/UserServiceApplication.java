/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.education.cloud;

import com.education.cloud.util.annotation.EnableCustomFeignClient;
import com.education.cloud.util.annotation.EnableServerProtect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * 用户服务
 */

@EnableCustomFeignClient
@EnableServerProtect
@EnableFeignClients
@ServletComponentScan
@SpringCloudApplication
public class UserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}

}
