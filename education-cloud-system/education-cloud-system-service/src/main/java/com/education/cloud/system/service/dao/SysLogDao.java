package com.education.cloud.system.service.dao;

import com.education.cloud.system.service.dao.impl.mapper.entity.SysLog;
import com.education.cloud.system.service.dao.impl.mapper.entity.SysLogExample;
import com.education.cloud.util.base.Page;

public interface SysLogDao {
    int save(SysLog record);

    int deleteById(Long id);

    int updateById(SysLog record);

    int updateByExampleSelective(SysLog record, SysLogExample example);

    SysLog getById(Long id);

    Page<SysLog> listForPage(int pageCurrent, int pageSize, SysLogExample example);
}
